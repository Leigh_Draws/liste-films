# Liste de film

**- Afficher une liste de film à partir d'un tableau dans la page**
- Possibilité d'ajouter un nouveau film grâce au formulaire (ajout dans le tableau d'objet?)
- Possibilité de supprimer un film via un bouton (suppression d'un objet présent dans le tableau)
- Utiliser localStorage afin de garder en mémoire les modifications faites par l'utilisateur