

// movieList est un tableau d'objets, chaque film est un objet avec des propriétés
const movieList = [
    {
      title: "Avatar",
      releaseYear: 2009,
      duration: 162,
      director: "James Cameron",
      actors: ["Sam Worthington","Zoe Saldana", "Sigourney Weaver", "Stephen Lang"],
      description: "A paraplegic marine dispatched to the moon Pandora on a unique mission becomes torn between following his orders and protecting the world he feels is his home.",
      poster: "https://m.media-amazon.com/images/M/MV5BZDA0OGQxNTItMDZkMC00N2UyLTg3MzMtYTJmNjg3Nzk5MzRiXkEyXkFqcGdeQXVyMjUzOTY1NTc@._V1_SX300.jpg",
      rating: 7.9,
    },
    {
      title: "300",
      releaseYear: 2006,
      duration: 117,
      director: "Zack Snyder",
      actors: ["Gerard Butler", "Lena Headey", "Dominic West", "David Wenham"],
      description: "King Leonidas of Sparta and a force of 300 men fight the Persians at Thermopylae in 480 B.C.",
      poster: "https://m.media-amazon.com/images/M/MV5BMjc4OTc0ODgwNV5BMl5BanBnXkFtZTcwNjM1ODE0MQ@@._V1_SX300.jpg",
      rating: 7.7,
    },
    {
      title: "The Avengers",
      releaseYear: 2012,
      duration: 143,
      director: "Joss Whedon",
      actors: ["Robert Downey Jr.", "Chris Evans", "Mark Ruffalo", "Chris Hemsworth"],
      description: "Earth's mightiest heroes must come together and learn to fight as a team if they are to stop the mischievous Loki and his alien army from enslaving humanity.",
      poster: "https://m.media-amazon.com/images/M/MV5BNDYxNjQyMjAtNTdiOS00NGYwLWFmNTAtNThmYjU5ZGI2YTI1XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg",
      rating: 8.1,
    },
  ];

const form = document.getElementById("formulaire");

const formTitle = document.getElementById("form-title");
const formDirector = document.getElementById("form-director");
const formDate = document.getElementById("form-date");
const formDuration = document.getElementById("form-duration");
const formActors = document.getElementById("form-actors")
const formDescription = document.getElementById("form-description");
const formPoster = document.getElementById("form-poster");
const formRating = document.getElementById("form-rating");

form.addEventListener('submit', submitForm);


function submitForm(event) {
  event.preventDefault();
  const newMovie = {
    title: formTitle.value,
    releaseYear: parseInt(formDate.value),
    duration: formDuration.value,
    director: formDirector.value,
    actors: formActors.value.split(","),
    description: formDescription.value,
    poster: formPoster.value,
    rating: formRating.value,
  }
  movieListContainer.textContent = " "
  movieList.push(newMovie);
  console.log(movieList)
  showMovieList()
}

// Récupère le conteneur dans lequel sera affiché la liste de film

let movieListContainer = document.getElementById("movies");

// Fonction pour afficher tous les éléments de chaque film un par un

function showMovieList() {

// Boucle pour chaque objet du tableau movieList

  for (const eachMovie of movieList) {

//Création d'un article avec la classe "card" 

    let movieCard = document.createElement("article");
    movieCard.classList.add("card");

//Création de chaque élément de chaque film (titre, directeur, etc ...)

    const movieTitle = document.createElement("h2");
    movieTitle.textContent = eachMovie.title;

    const moviePoster = document.createElement("img");
    moviePoster.src = eachMovie.poster;

    const movieDirector = document.createElement("h3");
    movieDirector.textContent = eachMovie.director;

    const movieYear = document.createElement("p");
    movieYear.textContent = "Sortie : " + eachMovie.releaseYear;

    const movieDuration = document.createElement("p");
    movieDuration.textContent = "Durée : "  + eachMovie.duration + " minutes.";

    const movieActors = document.createElement("p");
    movieActors.textContent = "Acteurs : " + eachMovie.actors.join(', ');

    const movieDescription = document.createElement("p");
    movieDescription.textContent = "Description : " + eachMovie.description;

    const movieNote = document.createElement("p");
    movieNote.textContent = "Note : " + eachMovie.rating + "/10";

// Ajoute chaque élément à movieCard

    movieCard.appendChild(movieTitle);
    movieCard.appendChild(moviePoster);
    movieCard.appendChild(movieDirector);
    movieCard.appendChild(movieYear);
    movieCard.appendChild(movieDuration);
    movieCard.appendChild(movieActors);
    movieCard.appendChild(movieDescription);
    movieCard.appendChild(movieNote)

// Ajoute movieCard au conteneur de la liste de films    

    movieListContainer.appendChild(movieCard)
}
}

// appelle la fonction afin d'afficher la liste

showMovieList()
